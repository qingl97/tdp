void cblas_dgemm_kij(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double *A,
                 const int lda, const double *B, const int ldb,
		     double *C, const int ldc);

void cblas_dgemm_ijk(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double *A,
                 const int lda, const double *B, const int ldb,
		     double *C, const int ldc);

void cblas_dgemm_jik(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double *A,
                 const int lda, const double *B, const int ldb,
		     double *C, const int ldc);
