#include "mycblas.h"
#include "util.h"
#include "perf.h"
#include "cblas.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv){
  
  int m;
  int i, j , k;
  
  FILE *outfile;
  outfile = fopen("testResults/multiplication_test.dat","w+");
  if(outfile == NULL){
    fprintf(stderr, "open file testResults/multiplication_test.dat failed!\n");
    exit(EXIT_FAILURE);
  }
  fprintf(outfile, "%-15s%-15s%-15s%-15s%-15s\n\n","# taille", "kij","ijk", "jik", "parallel"); 
 
  for(m = 100 ; m <=1000 ; m *= 1.25){
    /* Allocations de mémoires pour les matrices */
    double *A,*B,*C1,*C2,*C3,*C4, *C5;
    d_alloc_matrix(m,m,&A);
    d_alloc_matrix(m,m,&B);
    d_alloc_matrix(m,m,&C1);
    d_alloc_matrix(m,m,&C2);
    d_alloc_matrix(m,m,&C3);
    d_alloc_matrix(m,m,&C4);
    d_alloc_matrix(m,m,&C5);

    /*  Initialisation des matrices */
    d_init_matrix(m,m,C1,0);
    d_init_matrix(m,m,C2,0);
    d_init_matrix(m,m,C3,0);
    d_init_matrix(m,m,C4,0);
    d_init_matrix(m,m,C5,0);

    d_init_matrix(m,m,A,1);
    d_init_matrix(m,m,B,2);
    
    /*Affichage de A et B */ 
    //d_affichage(m,m,A,m,stdout);
    //d_affichage(m,m,B,m,stdout);
    
    
    perf_t start;
    perf_t stop;
    double mflops;
    
    /* test performance kij */
    
    perf(&start);
    cblas_matrixMultiplication_kij(m,m,m,A,m,B,m,C1,m);  
    perf(&stop);
    
    perf_diff(&start,&stop);
    
    mflops = perf_mflops(&stop, 1000000);
    fprintf(outfile, "%-15d%-15.4f",m,mflops);
    
    /* test performance ijk */ 
    perf(&start);
    cblas_matrixMultiplication_kij(m,m,m,A,m,B,m,C2,m);  
    perf(&stop);
    
    perf_diff(&start,&stop);
    
    mflops = perf_mflops(&stop, 1000000);
    fprintf(outfile, "%-15.4f",mflops);
    
    /* test performance jik */
    perf(&start);
    cblas_matrixMultiplication_kij(m,m,m,A,m,B,m,C3,m);  
    perf(&stop);
    
    perf_diff(&start,&stop);
    
    mflops = perf_mflops(&stop, 1000000);
    fprintf(outfile, "%-15.4f",mflops);
    

    /* test parallel*/
    perf(&start);
    cblas_matrixMultiplication_Parallel(m,m,m,A,m,B,m,C4,m);  
    perf(&stop);
    
    perf_diff(&start,&stop);
    
    mflops = perf_mflops(&stop, 1000000);
    fprintf(outfile, "%-15.4f\n",mflops);
       
    /* Liberation Memoire */
    free(A);
    free(B);
    free(C1);
    free(C2);
    free(C3);
    free(C4);
  }

  return 0;
}
