set xlabel "Taille de la matrice"
set ylabel "Mflops"
set xrange [0:400]
set yrange [0:1200]
set term png
set output 'dgemm_perf.png'

plot "dgemm_test.dat" using 1:2 title 'dgemm_scalaire' with lines,\
     "dgemm_test.dat" using 1:3 title 'dgemm_par_bloc' with lines


