set xlabel "Taille de la matrice"
set ylabel "Mflops"
set autoscale
set term png
set output 'multiplication_perf.png'

plot "multiplication_test.dat" using 1:2 title 'multi_kij' with lines,\
     "multiplication_test.dat" using 1:3 title 'multi_ijk' with lines,\
     "multiplication_test.dat" using 1:4 title 'multi_jik' with lines,\
     "multiplication_test.dat" using 1:5 title 'multi_parallel' with lines
