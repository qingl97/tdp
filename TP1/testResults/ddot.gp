set xlabel "Taille du vecteur"
set ylabel "Mflops"
set xrange [0:100000]
set yrange [0:200000]

set term png
set output 'ddot_perf.png'

plot "ddot_test.dat" using 1:2 title 'ddot' with lines
