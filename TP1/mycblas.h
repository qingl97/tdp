#ifndef MYCBLAS_H
#define MYCBLAS_H

#include "cblas.h"
#include <stdio.h>
#include <assert.h>
#include <pthread.h>

struct threadArg{
  int startLine;
  int nbLines;
  int M,N,K;
  const double* A;
  const double* B;
  double *C;
  int lda,ldb,ldc;
};


// Multiplication de matrices
void cblas_matrixMultiplication_kij(const int M, const int N,
				    const int K, const double *A,
				    const int lda, const double *B, const int ldb,
				    double *C, const int ldc);

void cblas_matrixMultiplication_ijk(const int M, const int N,
				    const int K, const double *A,
				    const int lda, const double *B, const int ldb,
				    double *C, const int ldc);

void cblas_matrixMultiplication_jik(const int M, const int N,
				    const int K, const double *A,
				    const int lda, const double *B, const int ldb,
				    double *C, const int ldc);


void cblas_matrixMultiplication_Parallel(const int M, const int N,
					 const int K, const double *A,
					 const int lda, const double *B, const int ldb,
					 double *C, const int ldc);

/* Completion de la bilbiothèque CBLAS */

void cblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY);

void cblas_dgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY);

void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc);

void cblas_dger(const enum CBLAS_ORDER Order, const int M, const int N,
                const double alpha, const double *X, const int incX,
                const double *Y, const int incY, double *A, const int lda);

//Optionnels
void cblas_saxpy(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY);

void cblas_sgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY);

void cblas_sgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc);

void cblas_sger(const enum CBLAS_ORDER Order, const int M, const int N,
                const float alpha, const float *X, const int incX,
                const float *Y, const int incY, float *A, const int lda);

#endif
