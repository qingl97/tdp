#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <stdio.h>

/* Affichage matrices */
void s_affichage(const int rows,const int column, float* a, int lda, FILE* flux);
void d_affichage(const int rows,const int column, double* a, int lda, FILE* flux);

/* Allocation de memoire pour vecteurs et matrices */
void s_alloc_vector(const int n, float** a);
void s_alloc_matrix(const int m, const int n, float** a);
void d_alloc_vector(const int n, double** a);
void d_alloc_matrix(const int m, const int n, double** a);

/* Initialisation d'un vecteur ou d'une matrice avec val */ 
void s_init_vector(const int n, float* a, const float val);
void s_init_matrix(const int m, const int n, float* a, const float val);
void d_init_vector(const int n, double* a, const double val);
void d_init_matrix(const int m, const int n, double* a, const double val);


#endif
