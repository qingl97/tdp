#include "util.h"

#include "util.h"

void s_affichage(int rows, int column, float* a, int lda, FILE* flux){
  int i,j;
  fprintf(flux,"\n");
  for(i = 0 ; i < rows ; i++){
    for(j = 0; j < column ; j++){
      fprintf(flux,"%lf ",a[j*lda+i]);
    }
    fprintf(flux,"\n");
  }
}
void d_affichage(int rows, int column, double* a, int lda, FILE* flux){
  int i,j;
  fprintf(flux,"\n");
  for(i = 0 ; i < rows ; i++){
    for(j = 0; j < column ; j++){
      fprintf(flux,"%lf ",a[j*lda+i]);
    }
    fprintf(flux,"\n");
  }
}


///////////////////////////////////////////////////////////

void s_alloc_vector(const int n, float** a){
  *a = malloc(n*sizeof(float));
}

void s_alloc_matrix(const int m,const  int n, float **a){
  *a = malloc(n*m*sizeof(float));
}

void d_alloc_vector(const int n, double** a){
  *a = malloc(n*sizeof(double));
}

void d_alloc_matrix(const int m, const  int n, double **a){
  *a = malloc(n*m*sizeof(double));
}

///////////////////////////////////////////////////////////

void s_init_vector(const int n, float* a, const float val){
  int i;
  for(i = 0; i < n ; i++)
    a[i] = val; 
}
void d_init_vector(const int n, double* a, const double val){
  int i;
  for(i = 0; i < n ; i++)
    a[i] = val;
}
void s_init_matrix(const int m, const int n, float* a, const float val){
  int i;
  for(i = 0; i < n*m ; i++)
    a[i] = val;
}
void d_init_matrix(const int m, const int n, double* a, const double val){
  int i;
  for(i = 0; i < n*m ; i++)
    a[i] = val;
}
