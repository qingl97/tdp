#include "ddot.h"
#include "perf.h"
#include "cblas.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv){

  FILE *outfile;
  outfile = fopen("testResults/ddot_test.dat", "w+");
  if(outfile == NULL){
    fprintf(stderr, "open file testResults/ddot_test.dat failed!\n");
    exit(EXIT_FAILURE);
   }
  
  fprintf(outfile, "%-15s%-15s\n\n","# taille(M)","perf_ddot");

  int N;
  for(N=50; N < 1000000; N = (5*N)/4){
    
    double * X,*Y;
    d_alloc_vector(N,&X);
    d_alloc_vector(N,&Y);
    d_init_vector(N,X,1.0);
    d_init_vector(N,Y,2.0);

    int incX = 1, incY = 1;
    
    perf_t start;
    perf_t stop;
    double res;
    double mflops;

    /* evaluate the performance of ddot of this method */    
    perf(&start);
    res = ddot(N,X,incX,Y,incY);
    perf(&stop);
    
    perf_diff(&start,&stop);
    
    mflops = perf_mflops(&stop, 1000000);
    fprintf(outfile, "%-15d%-15.4f\n",N,mflops);

    /* evaluate the performance of ddot of MKL */
    //perf(&start);
    //res = cblas_ddot(N,X,incX,Y,incY);
    //perf(&stop);

    //perf_diff(&start, &stop);

    //mflops = perf_mflops(&stop, 1000000);
    //fprintf(outfile, "%-15.4f\n", mflops);

    free(X);
    free(Y);
  }

  fclose(outfile);
  
  return EXIT_SUCCESS;
}
