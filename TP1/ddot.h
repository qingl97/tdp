#ifndef _DDOT_H
#define _DDOT_H

double ddot(const int N, const double *X, const int incX,
                  const double *Y, const int incY);

#endif
