#include "mycblas.h"


/**************************************************************/
/* Matrix Multiplicaiton with different order of calculation  */
/**************************************************************/

void cblas_matrixMultiplication_kij(const int M, const int N,
                 const int K, const double *A,
                 const int lda, const double *B, const int ldb,
		     double *C, const int ldc){
  int i,j,k;

  for(k = 0 ; k < K ; k++){
    for(i = 0 ; i < M ; i++){
      for(j = 0 ; j < N ; j++){
	// c(i,j) = c(i,j) + a(k,i)*b(k,j)
	C[j*ldc+i] += A[i*lda+k]*B[j*ldb+k];
      }
    }
  }
}

void cblas_matrixMultiplication_ijk(const int M, const int N,
                 const int K, const double *A,
                 const int lda, const double *B, const int ldb,
		     double *C, const int ldc){
  int i,j,k;
  for(i = 0 ; i < M ; i++){
    for(j = 0 ; j < N ; j++){
      double tmp = 0;
      // tmp = sum k de a(k,i)*b(k,j)
      for(k = 0 ; k < K ; k++)
	tmp += A[i*lda+k]*B[j*ldb+k];
      C[j*ldc+i] = tmp;
    }
  }
}

void cblas_matrixMultiplication_jik(const int M, const int N,
                 const int K, const double *A,
                 const int lda, const double *B, const int ldb,
		     double *C, const int ldc){
  int i,j,k;
  for(j = 0 ; j < N ; j++){
    for(i = 0 ; i < M ; i++){
      double tmp = 0;
      // tmp = sum k de a(k,i)*b(k,j)
      for(k = 0 ; k < K ; k++)
	tmp += A[i*lda+k]*B[j*ldb+k];
      C[j*ldc+i] = tmp;
    }
  }
}

/**********************************************************/
/*            Matrix Multiplication in parallel           */
/**********************************************************/
void* execute(void*arg){

  struct threadArg *myarg = (struct threadArg *)arg;
  cblas_matrixMultiplication_jik(myarg->M,myarg->N,myarg->K,myarg->A,myarg->lda,myarg->B,myarg->ldb,myarg->C,myarg->ldc);
}



void cblas_matrixMultiplication_Parallel(const int M, const int N,
					 const int K, const double *A,
					 const int lda, const double *B, const int ldb,
					 double *C, const int ldc){
  int i;
  int nbThreads = 8;
  /*  char * tmp = NULL;
  tmp = getenv("MYLIB_NUM_THREADS");
  if(tmp != NULL){
    printf("\ncoucou\n",tmp);
    nbThreads = atoi(tmp);
    }*/
  assert(nbThreads > 0);

  pthread_t threads[nbThreads];
  struct threadArg myarg[nbThreads];
  
  // Initialisation des paramètre de chaque threads
  int q,r;
  q = (M)/nbThreads;
  r = M%nbThreads;
  for(i = 0 ; i < nbThreads ; i++){
    if(i < r){
      myarg[i].startLine = i*(q+1);
      myarg[i].nbLines = q+1;
    }
    else{
      myarg[i].startLine = r*(q+1) + (i-r)*q;
      myarg[i].nbLines = q;
    }
    myarg[i].A = &A[myarg[i].startLine];
    myarg[i].C = &C[myarg[i].startLine];
    myarg[i].B = B;

    myarg[i].lda = lda;
    myarg[i].ldb = ldb;
    myarg[i].ldc = ldc;

    myarg[i].M = myarg[i].nbLines;
    myarg[i].K = K;
    myarg[i].N = N;
  }
  
  for(i = 0 ; i < nbThreads ; i++)
    pthread_create(&threads[i],NULL,execute,&myarg[i]);
  for(i = 0 ; i < nbThreads ; i++)
    pthread_join(threads[i],NULL);

}

/**********************************************/
/*       Optional implementations of BLAS     */
/**********************************************/

void cblas_daxpy(const int N, const double alpha, const double *X,
                 const int incX, double *Y, const int incY){
  if(N <= 0 || alpha == 0)
    return;
  
  if(incX < 0 || incY < 0)
    return;

  int i;
  for(i = 0 ; i < N ; i++)
    Y[incY*i] = alpha*Y[incY*i] + X[i*incX];  
}

void cblas_dgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const double alpha, const double *A, const int lda,
                 const double *X, const int incX, const double beta,
                 double *Y, const int incY){
  int i,j;
  assert(Order == CblasColMajor);
  assert( TransA == CblasNoTrans);
  
  for(i = 0 ; i < M  ; i++){
    Y[i*incY] = beta*Y[i*incY];
    double tmp = 0.0;
    for(j = 0 ; j < N ; j++)
      tmp += A[j*lda+i]*X[j*incX];
    Y[i*incY] += tmp;
  }
}

void cblas_dgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc){
  assert(Order == CblasColMajor);
  assert(TransA == CblasNoTrans && TransB == CblasNoTrans);

  if(alpha == 0 && beta == 0)
    return;
  // C = beta*C + alpha*(tA.B)
  int i,j,k;
  for(j=0 ; j < N ; j++){
    for(i= 0; i < M ; i++){
      double tmp = 0.0;
      for(k=0 ; k < K ; k++)
	tmp += A[k*lda+i]*B[j*ldb+k];
      
      C[j*ldc+i] = alpha*tmp+beta*C[j*ldc+i];
    }
  }
}

void cblas_dger(const enum CBLAS_ORDER Order, const int M, const int N,
                const double alpha, const double *X, const int incX,
                const double *Y, const int incY, double *A, const int lda){
  
  assert(Order == CblasColMajor);
  
  if(alpha == 0 || incX <= 0 || incY <= 0)
    return;

  int i,j;
  for(i = 0 ; i < M ; i++){
    for(j = 0 ; j < N ; j++){
      A[j*lda+i] += alpha*X[i*incX]*Y[j*incY];
    }
  }
}

void cblas_saxpy(const int N, const float alpha, const float *X,
                 const int incX, float *Y, const int incY){
 if(N <= 0 || alpha == 0)
    return;
  
  if(incX < 0 || incY < 0)
    return;

  int i;
  for(i = 0 ; i < N ; i++)
    Y[incY*i] = alpha*Y[incY*i] + X[i*incX];  
}

void cblas_sgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA, const int M, const int N,
                 const float alpha, const float *A, const int lda,
                 const float *X, const int incX, const float beta,
                 float *Y, const int incY){

  int i,j;
  assert(Order == CblasColMajor);
  assert( TransA == CblasNoTrans);
  
  for(i = 0 ; i < M  ; i++){
    Y[i*incY] = beta*Y[i*incY];
    double tmp = 0.0;
    for(j = 0 ; j < N ; j++)
      tmp += A[j*lda+i]*X[j*incX];
    Y[i*incY] += tmp;
  }
}

void cblas_sgemm(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const float alpha, const float *A,
                 const int lda, const float *B, const int ldb,
                 const float beta, float *C, const int ldc){
  
  assert(Order == CblasColMajor);
  assert(TransA == CblasNoTrans && TransB == CblasNoTrans);

  if(alpha == 0 && beta == 0)
    return;

  int i,j,k;
  for(j=0 ; j < N ; j++){
    for(i= 0; i < M ; i++){
      double tmp = 0.0;
      for(k=0 ; k < K ; k++)
	tmp += A[k*lda+i]*B[j*ldb+k];
      
      C[j*ldc+i] = alpha*tmp+beta*C[j*ldc+i];
    }
  }
}

void cblas_sger(const enum CBLAS_ORDER Order, const int M, const int N,
                const float alpha, const float *X, const int incX,
                const float *Y, const int incY, float *A, const int lda){

  assert(Order == CblasColMajor);
  
  if(alpha == 0 || incX <= 0 || incY <= 0)
    return;

  int i,j;
  for(i = 0 ; i < M ; i++){
    for(j = 0 ; j < N ; j++){
      A[j*lda+i] += alpha*X[i*incX]*Y[j*incY];
    }
  }
}
