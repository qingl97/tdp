#include "dgemm.h"

void cblas_dgemm_scalaire(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
                 const double beta, double *C, const int ldc){
 
 assert(Order == CblasColMajor);
 int i,j,k;
 for(j = 0 ; j < N ; j++){
   for(i = 0 ; i < M ; i++){
     // C = beta*C + alpha* (tA.B)
     double tmp = 0.0;
     for(k = 0 ; k < K ; k++){
       tmp += A[k*lda+i]*B[j*ldb+k]; // a(k,i) * b(k,j)
     }
     C[j*ldc+i] = beta*C[j*ldc+i] + alpha*tmp;
   }
 }
}

int numbOfBloc(const int M, const int N, const int K){
  int res = (M+N+K)/3;
  res = (int)sqrt(res);
  return res;
}

void cblas_dgemm_bloc(const enum CBLAS_ORDER Order, const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB, const int M, const int N,
                 const int K, const double alpha, const double *A,
                 const int lda, const double *B, const int ldb,
		      const double beta, double *C, const int ldc){

  assert(Order == CblasColMajor);
  int nbBloc = numbOfBloc(M,N,K);
  int i,j,k;
  int r_m = (M % nbBloc), r_n = (N % nbBloc), r_k = (K % nbBloc);
  int q_m = (M / nbBloc), q_n = (N / nbBloc), q_k = (K / nbBloc);
  for(i = 0 ; i < nbBloc ; i++){//Ligne
    for(j = 0 ; j < nbBloc ; j++){//Colonne
      for(k = 0 ; k < nbBloc ; k++){
	int M_tmp = (i < r_m)?(q_m+1):q_m;
	int N_tmp = (j < r_n)?(q_n+1):q_n;
	int K_tmp = (k < r_k)?(q_k+1):q_k;

	const double * A_tmp = &A[ ((k<r_k)?k*(q_k+1):(r_k*(q_k+1)+(k-r_k)*q_k))*lda + ((i<r_m)?i*(q_m+1):(r_m*(q_m+1)+(i-r_m)*q_m)) ];
	const double * B_tmp = &B[ ((j<r_n)?j*(q_n+1):(r_n*(q_n+1)+(j-r_n)*q_n))*ldb + ((k<r_k)?k*(q_k+1):(r_k*(q_k+1)+(k-r_k)*q_k))];
	double * C_tmp = &C[ ((j<r_n)?j*(q_n+1):(r_n*(q_n+1)+(j-r_n)*q_n))*ldc + ((i<r_m)?i*(q_m+1):(r_m*(q_m+1)+(i-r_m)*q_m)) ];

	cblas_dgemm_scalaire(Order,TransA,TransB,M_tmp,N_tmp,K_tmp,alpha,A_tmp,lda,B_tmp,ldb,beta,C_tmp,ldc);
      }
    }
  }

}
