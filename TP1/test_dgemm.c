#include "util.h"
#include "perf.h"
#include "cblas.h"
#include "dgemm.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv){
  
  int m;
  int i, j , k;
 
  FILE *outfile;
  outfile = fopen("testResults/dgemm_test.dat","w+");
  if(outfile == NULL){
    fprintf(stderr, "open file testResults/dgemm_test.dat failed!\n");
    exit(EXIT_FAILURE);
  }
  fprintf(outfile, "%-15s%-15s%-15s\n\n","# taille", "dgemm_scalaire","dgemm_bloc"); 
  
  for(m = 50 ; m < 1000 ; m*=1.25){
    double *A,*B,*C1,*C2, *C3;
    d_alloc_matrix(m,m,&A);
    d_alloc_matrix(m,m,&B);
    d_alloc_matrix(m,m,&C1);
    d_alloc_matrix(m,m,&C2);
    //d_alloc_matrix(m,m,&C3);
    
    d_init_matrix(m,m,A,1);
    d_init_matrix(m,m,B,2);
    d_init_matrix(m,m,C1,0);
    d_init_matrix(m,m,C2,0);
    //d_init_matrix(m,m,C3,0);

    perf_t start;
    perf_t stop;
    double mflops;
    
    /* test performance scalaire */
    
    perf(&start);
    cblas_dgemm_scalaire(CblasColMajor,CblasNoTrans,CblasNoTrans,m,m,m,1,A,m,B,m,1,C1,m);
    perf(&stop);
    
    perf_diff(&start,&stop);
    
    mflops = perf_mflops(&stop, 1000000);
    fprintf(outfile, "%-15d%-15.4f",m,mflops);
  
    /* test performance dgemm bloc en order ijk */ 
    perf(&start);
    cblas_dgemm_bloc(CblasColMajor,CblasNoTrans,CblasNoTrans,m,m,m,1,A,m,B,m,1,C2,m);
    perf(&stop);
    
    perf_diff(&start,&stop);
    
    mflops = perf_mflops(&stop, 1000000);
    fprintf(outfile, "%-15.4f\n",mflops);
   
    
    /* test performance dgemm using MKL*/
    //perf(&start);
    //cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,m,m,m,1,A,m,B,m,1,C3,m);
    //perf(&stop);
    
    // perf_diff(&start,&stop);
    
    //mflops = perf_mflops(&stop, 1000000);
    //fprintf(outfile, "%-15.4f\n",mflops);

    free(A);
    free(B);
    free(C1);
    free(C2);
    //free(C3);
  }
  
  fclose(outfile);
  return 0;

}
